<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="webContext" value="${pageContext.request.contextPath}" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Portfolio</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">

<link rel="stylesheet"
	href="${webContext}/resources/css/font-awesome.css" />
<link rel="stylesheet"
	href="${webContext}/resources/css/bootstrap.min.css" />

<script type="text/javascript"
	src="${webContext}/resources/js/plugins/jquery.min.js"></script>
<script type="text/javascript"
	src="${webContext}/resources/js/plugins/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${webContext}/resources/js/portfolio.js"></script>
	
<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<script>
	var ctx = '${webContext}';
</script>

</head>
<body>
	<div id="panelPortfolio" class="form-group">
		<div class="row">
			<div class="col-lg-5">
				<div class="row">
					<div class="col-lg-12">
						<img id="picture" src="" alt="Mountain View" />
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div id="tweets"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="row">
					<div class="col-lg-12">
						<h1 id="title"></h1>
						<br></br>
						<p id="description" align="justify"></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>