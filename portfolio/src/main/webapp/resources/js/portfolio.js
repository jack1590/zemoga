$(document).on("ready",function(){
	objPortfolio.initComponents();
	objPortfolio.getPortfolio();
});

var objPortfolio = {
		
	initComponents : function(){
		//$("#panelPortfolio").hide();
	},
		
	getPortfolio : function(){
	    url= ctx+"/zemoga_portfolio_api/user_info/0";
	    $.ajax({
	        url: url,
	        type: 'GET',
	        dataType: 'json',
	        success:function(objResponse){
	        	
	        	objPortfolio.printInformation(objResponse);
	        	
	        	
	        },
	        error:function(objRequest){
	            alert("danger");
	        }
	     });
	},
	

	printInformation : function(objResponse){
		$("#picture").prop("src", objResponse.imageUrl);
    	$("#picture").prop("alt", objResponse.title);
    	$("#title").text(objResponse.title);
    	$("#description").text(objResponse.description)
    	
    	var tweets = "";
    	
    	for(var i = 0; i<5; i++){
    		tweets += "<blockquote class=\"twitter-tweet\"><p>"+objResponse.tweets[i]+"</p></blockquote>"
    	}
    	
    	$("#tweets").html(tweets);
	}
}