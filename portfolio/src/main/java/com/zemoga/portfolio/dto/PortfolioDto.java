package com.zemoga.portfolio.dto;

import java.util.ArrayList;
import java.util.List;


public class PortfolioDto {
	
	private Long id;
	
	private String imageUrl;
	
	private String title;
	
	private String description;
	
	private String twitterUserName;
	
	private List<String> tweets;

	public PortfolioDto(){}
	
	public PortfolioDto(Long id, String imageUrl, String title,
			String description, String twitterUserName) {
		super();
		this.id = id;
		this.imageUrl = imageUrl;
		this.title = title;
		this.description = description;
		this.twitterUserName = twitterUserName;
		this.tweets = new ArrayList<>();
	}
	
	public void addTweets(List<String> tweets){
		this.tweets.addAll(tweets);
	}

	public Long getId() {
		return id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getTwitterUserName() {
		return twitterUserName;
	}

	public List<String> getTweets() {
		return tweets;
	}
}
