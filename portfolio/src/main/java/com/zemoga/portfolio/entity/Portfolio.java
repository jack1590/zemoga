package com.zemoga.portfolio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="portfolio")
@NamedQueries({
	@NamedQuery(name="Portfolio.findAll", query="SELECT NEW com.zemoga.portfolio.dto.PortfolioDto(p.id,p.imageUrl,p.title,p.description, p.twitterUserName) FROM Portfolio p "),
	@NamedQuery(name="Portfolio.findById", query="SELECT NEW com.zemoga.portfolio.dto.PortfolioDto(p.id,p.imageUrl,p.title,p.description, p.twitterUserName) FROM Portfolio p WHERE p.id = :id ")
})
public class Portfolio {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idportfolio")
	private Long id;
	
	@Column(name="imageURL")
	private String imageUrl;
	
	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@Column(name="twitterUserName")
	private String twitterUserName;
	
	public Portfolio(){}
	
	public Portfolio(Long id){
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
