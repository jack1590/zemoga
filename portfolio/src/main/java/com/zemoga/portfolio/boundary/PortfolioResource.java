package com.zemoga.portfolio.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zemoga.portfolio.control.PortfolioControl;
import com.zemoga.portfolio.control.TwitterClient;
import com.zemoga.portfolio.dto.PortfolioDto;

@RestController
@RequestMapping(value = "zemoga_portfolio_api/")
public class PortfolioResource {

	@Autowired
	PortfolioControl control;

	@Autowired
	TwitterClient twitterClient;
	
	@RequestMapping(value = "/user_info")
	public PortfolioDto findUserInfo() {
		return this.findUserInfo(0L);
	}

	@RequestMapping(value = "/user_info/{id}")
	public PortfolioDto findUserInfo(@PathVariable Long id) {
		PortfolioDto portfolio = this.control.findUserInfo(id);
		portfolio.addTweets(twitterClient.consultarTweetsByNick(portfolio
				.getTwitterUserName()));
		
		return portfolio;
	}
	
	@RequestMapping(value = "/modify_user_info", method=RequestMethod.POST)
	public String updateUserInfo(@RequestBody PortfolioDto portfolio) {
		this.control.updateUserInfo(portfolio);
		return "The information was updated successfully";
	}
}
