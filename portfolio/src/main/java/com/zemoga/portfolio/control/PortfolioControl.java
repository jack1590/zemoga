package com.zemoga.portfolio.control;

import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.zemoga.portfolio.dto.PortfolioDto;
import com.zemoga.portfolio.entity.Portfolio;

@Component
public class PortfolioControl {

	@PersistenceContext
	EntityManager em;

	public PortfolioDto findUserInfo(Long id) {
		return em.createNamedQuery("Portfolio.findById", PortfolioDto.class)
				.setParameter("id", id).getSingleResult();
	}

	@Transactional
	public void updateUserInfo(PortfolioDto portfolio) {
		
		Portfolio entity = em.find(Portfolio.class, 0L);
		
		if (!Objects.isNull(portfolio.getTitle())
				&& !portfolio.getTitle().isEmpty()) {
			entity.setTitle(portfolio.getTitle());
		}
		
		if (!Objects.isNull(portfolio.getDescription())
				&& !portfolio.getDescription().isEmpty()) {
			entity.setDescription(portfolio.getDescription());
		}
		
		if (!Objects.isNull(portfolio.getImageUrl())
				&& portfolio.getImageUrl().isEmpty()) {
			entity.setImageUrl(portfolio.getImageUrl());
		}
		
		em.merge(entity);
	}
	
}
