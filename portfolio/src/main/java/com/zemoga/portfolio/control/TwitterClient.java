package com.zemoga.portfolio.control;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@Component
public class TwitterClient {

	ConfigurationBuilder cb;
	TwitterFactory tf;

	public TwitterClient(){
		autenticar();
	}
	
	public List<String> consultarTweetsByNick(String nickName){
		Twitter twitter = tf.getInstance();
		try {
			return twitter.getUserTimeline(nickName).stream().parallel()
					.map(tw -> tw.getText()).collect(Collectors.toList());
		} catch (TwitterException e) {
			return new ArrayList<>();
		}

	}

	public void autenticar() {
		cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
				.setOAuthConsumerKey("ag8Ll5Z1O9TQPoBtMZXNoKFKY")
				.setOAuthConsumerSecret(
						"kwtGdesRr9iVCwz28hZtiqCAXe1BZSJ5cmmb9J4rmawoXSc9fP")
				.setOAuthAccessToken(
						"2789965944-qqqRjYz9bITYuuGddHsdZC98TKA65hr0OrYKsJ5")
				.setOAuthAccessTokenSecret(
						"1opSAwsZkhELNBfiIYQc7X16MQy8WZKXMWgoBZHmCLOYR");
		tf = new TwitterFactory(cb.build());
	}
}
